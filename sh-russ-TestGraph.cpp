// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

// add_vertex() and num_vertices()
TYPED_TEST(GraphFixture, test5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    ASSERT_EQ(num_vertices(g), 3);

    ASSERT_EQ(vertex(0,g), vdA);
    ASSERT_EQ(vertex(1,g), vdB);
    ASSERT_EQ(vertex(2,g), vdC);
}

// vertices() and vertex iterators
TYPED_TEST(GraphFixture, test6) {
    using graph_type          = typename TestFixture::graph_type;
    graph_type g;
    for(int i = 0 ; i < 20; ++i) add_vertex(g);
    auto p = vertices(g);
    auto b = p.first;
    auto e = p.second;
    int i = 0;
    while(b != e) {
        ASSERT_EQ(vertex(i,g), i);
        ++b;
        ++i;
    }
}

// add_edge() and num_edges()
TYPED_TEST(GraphFixture, test7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    ASSERT_TRUE(add_edge(vdA, vdB, g).second);
    ASSERT_TRUE(add_edge(vdB, vdC, g).second);
    ASSERT_TRUE(add_edge(vdA, vdC, g).second);

    ASSERT_EQ(num_edges(g), 3);
}

// edge()
TYPED_TEST(GraphFixture, test8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);

    ASSERT_TRUE(edge(vdA, vdB, g).second);
    ASSERT_TRUE(edge(vdB, vdC, g).second);
    ASSERT_FALSE(edge(vdA, vdC, g).second);
}


// trying to add edges that already exist
TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    ASSERT_EQ(num_edges(g), 0);
    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdA, vdC, g);
    ASSERT_EQ(num_edges(g), 3);

    ASSERT_FALSE(add_edge(vdA, vdB, g).second);
    ASSERT_FALSE(add_edge(vdB, vdC, g).second);
    ASSERT_FALSE(add_edge(vdA, vdC, g).second);
    ASSERT_EQ(num_edges(g), 3);
}

// edges and edge iterators
TYPED_TEST(GraphFixture, test10) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    vector<edge_descriptor> v;
    edge_descriptor e1 = add_edge(vdA, vdB, g).first;
    edge_descriptor e2 = add_edge(vdA, vdC, g).first;
    edge_descriptor e3 = add_edge(vdB, vdC, g).first;

    edge_iterator b = edges(g).first;
    edge_iterator e = edges(g).second;
    ASSERT_EQ(*b, e1);
    ++b;
    ASSERT_EQ(*b, e2);
    ++b;
    ASSERT_EQ(*b, e3);
    ++b;
    ASSERT_EQ(b, e);
}

//target and source
TYPED_TEST(GraphFixture, test11) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);


    edge_descriptor e1 = add_edge(vdA, vdB, g).first;
    edge_descriptor e2 = add_edge(vdA, vdC, g).first;
    edge_descriptor e3 = add_edge(vdB, vdC, g).first;
    edge_descriptor e4 = add_edge(vdB, vdA, g).first;

    ASSERT_EQ(source(e1,g), vdA);
    ASSERT_EQ(target(e1,g), vdB);
    ASSERT_EQ(source(e2,g), vdA);
    ASSERT_EQ(target(e2,g), vdC);
    ASSERT_EQ(source(e3,g), vdB);
    ASSERT_EQ(target(e3,g), vdC);
    ASSERT_EQ(source(e4,g), vdB);
    ASSERT_EQ(target(e4,g), vdA);
}

//adjacent vertices
TYPED_TEST(GraphFixture, test12) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdA, vdD, g);
    add_edge(vdB, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;

    ASSERT_EQ(*b, vdB);
    ++b;
    ASSERT_EQ(*b, vdC);
    ++b;
    ASSERT_EQ(*b, vdD);
    ++b;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdB, g);
    b = p.first;
    e = p.second;
    ASSERT_EQ(*b, vdA);
    ++b;
    ASSERT_EQ(b,e);

    p = adjacent_vertices(vdC, g);
    b = p.first;
    e = p.second;

    ASSERT_EQ(b,e);
}

//add edge, (if vertices dont exist they should be created)
TYPED_TEST(GraphFixture, test13) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;

    add_edge(vertex(0,g), vertex(5,g), g);

    ASSERT_EQ(num_vertices(g), 6);
    ASSERT_EQ(num_edges(g), 1);
}

//cyclic graph
TYPED_TEST(GraphFixture, test14) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor e1 = add_edge(vdA, vdB, g).first;
    edge_descriptor e2 = add_edge(vdB, vdC, g).first;
    edge_descriptor e3 = add_edge(vdC, vdA, g).first;


    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;

    ASSERT_EQ(*b, e1);
    ++b;
    ASSERT_EQ(*b, e2);
    ++b;
    ASSERT_EQ(*b, e3);
    ++b;
    ASSERT_EQ(b,e);
}

//graph with loop
TYPED_TEST(GraphFixture, test15) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    edge_descriptor e1 = add_edge(vdA, vdA, g).first;


    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;
    ASSERT_EQ(num_edges(g), 1);
    ASSERT_EQ(*b, e1);
    ++b;
    ASSERT_EQ(b,e);
}

//graph with loop
TYPED_TEST(GraphFixture, test16) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    edge_descriptor e1 = add_edge(vdA, vdA, g).first;
    ASSERT_EQ(num_vertices(g), 1);
    ASSERT_EQ(num_edges(g), 1);
    ASSERT_EQ(target(e1,g),source(e1,g));
}

//graph with loops
TYPED_TEST(GraphFixture, test17) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor e1 = add_edge(vdA, vdA, g).first;
    edge_descriptor e2 = add_edge(vdB, vdB, g).first;
    edge_descriptor e3 = add_edge(vdC, vdC, g).first;
    ASSERT_FALSE(add_edge(vdA, vdA, g).second);


    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;

    ASSERT_EQ(num_edges(g), 3);
    ASSERT_EQ(*b, e1);
    ++b;
    ASSERT_EQ(*b, e2);
    ++b;
    ASSERT_EQ(*b, e3);
    ++b;
    ASSERT_EQ(b,e);
}

//full graph with loops
TYPED_TEST(GraphFixture, test18) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor e1 = add_edge(vdA, vdA, g).first;
    edge_descriptor e2 = add_edge(vdA, vdB, g).first;
    edge_descriptor e3 = add_edge(vdB, vdA, g).first;
    edge_descriptor e4 = add_edge(vdB, vdB, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;

    ASSERT_EQ(num_edges(g), 4);
    ASSERT_EQ(*b, e1);
    ++b;
    ASSERT_EQ(*b, e2);
    ++b;
    ASSERT_EQ(*b, e3);
    ++b;
    ASSERT_EQ(*b, e4);
    ++b;
    ASSERT_EQ(b,e);
}

//graph with loops, adjacent vertices
TYPED_TEST(GraphFixture, test19) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdA, g).first;
    add_edge(vdA, vdB, g).first;
    add_edge(vdB, vdA, g).first;
    add_edge(vdB, vdB, g).first;

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;

    ASSERT_EQ(*b, vdA);
    ++b;
    ASSERT_EQ(*b, vdB);
    ++b;
    ASSERT_EQ(b, e);


    p = adjacent_vertices(vdB, g);
    b = p.first;
    e = p.second;

    ASSERT_EQ(*b, vdA);
    ++b;
    ASSERT_EQ(*b, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

//vertices on empty graph
TYPED_TEST(GraphFixture, test20) {
    using graph_type          = typename TestFixture::graph_type;
    graph_type g;
    auto p = vertices(g);
    auto b = p.first;
    auto e = p.second;
    ASSERT_EQ(b, e);
}

//edges on empty graph
TYPED_TEST(GraphFixture, test21) {
    using graph_type          = typename TestFixture::graph_type;
    graph_type g;
    auto p = edges(g);
    auto b = p.first;
    auto e = p.second;
    ASSERT_EQ(b, e);
}

//adjacent vertices, no edges
TYPED_TEST(GraphFixture, test22) {
    using graph_type          = typename TestFixture::graph_type;
    graph_type g;
    add_vertex(g);
    auto p = adjacent_vertices(vertex(0,g),g);
    auto b = p.first;
    auto e = p.second;
    ASSERT_EQ(b, e);
}

//add edge, (if vertices dont exist they should be created)
TYPED_TEST(GraphFixture, test23) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;

    //order opposite other add edge test
    add_edge(vertex(5,g), vertex(0,g), g);

    ASSERT_EQ(num_vertices(g), 6);
    ASSERT_EQ(num_edges(g), 1);
}

//add_vertex many times
TYPED_TEST(GraphFixture, test24) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    for(int i = 0; i < 1000; ++i) {
        add_vertex(g);
    }
    for(int i = 0; i < 1000; ++i) {
        ASSERT_EQ(i, vertex(i,g));
    }
    ASSERT_EQ(num_vertices(g), 1000);
}

//add_edge many times
TYPED_TEST(GraphFixture, test25) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    for(int i = 0; i < 1000; ++i) {
        add_vertex(g);
    }
    for(int i = 0; i < 999; ++i) {
        add_edge(vertex(i,g), vertex(i+1,g), g);
    }
    ASSERT_EQ(num_vertices(g), 1000);
    ASSERT_EQ(num_edges(g), 999);
}

//target, source many times
TYPED_TEST(GraphFixture, test26) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;
    for(int i = 0; i < 1000; ++i) {
        add_vertex(g);
    }
    for(int i = 0; i < 999; ++i) {
        edge_descriptor e1 = add_edge(vertex(i,g), vertex(i+1,g), g).first;
        ASSERT_EQ(source(e1, g), vertex(i,g));
        ASSERT_EQ(target(e1, g), vertex(i+1,g));
    }
    ASSERT_EQ(num_vertices(g), 1000);
    ASSERT_EQ(num_edges(g), 999);
}

//add_edge many times (vertex doesnt exist yet)
TYPED_TEST(GraphFixture, test27) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;
    for(int i = 0; i < 999; ++i) {
        edge_descriptor e1 = add_edge(vertex(i,g), vertex(i+1,g), g).first;
        ASSERT_EQ(source(e1, g), vertex(i,g));
        ASSERT_EQ(target(e1, g), vertex(i+1,g));
    }
    ASSERT_EQ(num_vertices(g), 1000);
    ASSERT_EQ(num_edges(g), 999);
}

//add_edge many times (vertex doesnt exist yet)
TYPED_TEST(GraphFixture, test28) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    graph_type g;
    for(int i = 0; i < 999; ++i) {
        edge_descriptor e1 = add_edge(vertex(i,g), vertex(i+1,g), g).first;
        ASSERT_EQ(source(e1, g), vertex(i,g));
        ASSERT_EQ(target(e1, g), vertex(i+1,g));
    }
    ASSERT_EQ(num_vertices(g), 1000);
    ASSERT_EQ(num_edges(g), 999);
}


//add_edge fails
TYPED_TEST(GraphFixture, test29) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    for(int i = 0; i < 1000; ++i) {
        add_vertex(g);
    }
    for(int i = 0; i < 999; ++i) {
        add_edge(vertex(i,g), vertex(i+1,g), g);
    }
    for(int i = 0; i < 999; ++i) {
        ASSERT_FALSE(add_edge(vertex(i,g), vertex(i+1,g), g).second);
    }
    ASSERT_EQ(num_vertices(g), 1000);
    ASSERT_EQ(num_edges(g), 999);
}

//edge
TYPED_TEST(GraphFixture, test30) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    for(int i = 0; i < 1000; ++i) {
        add_vertex(g);
    }
    for(int i = 0; i < 999; i += 2) {
        add_edge(vertex(i,g), vertex(i+1,g), g);
    }
    for(int i = 0; i < 999; i += 2) {
        ASSERT_TRUE(edge(vertex(i,g), vertex(i+1, g), g).second);
    }
    ASSERT_EQ(num_vertices(g), 1000);
    ASSERT_EQ(num_edges(g), 500);
}

//edge
TYPED_TEST(GraphFixture, test31) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    for(int i = 0; i < 1000; ++i) {
        add_vertex(g);
    }
    for(int i = 0; i < 999; i += 2) {
        add_edge(vertex(i,g), vertex(i+1,g), g);
    }
    for(int i = 1; i < 999; i += 2) {
        ASSERT_FALSE(edge(vertex(i,g), vertex(i+1, g), g).second);
    }
    ASSERT_EQ(num_vertices(g), 1000);
    ASSERT_EQ(num_edges(g), 500);
}

//edge
TYPED_TEST(GraphFixture, test32) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    for(int i = 0; i < 1000; ++i) {
        add_vertex(g);
    }
    for(int i = 0; i < 999; i += 2) {
        add_edge(vertex(i,g), vertex(i+1,g), g);
    }
    for(int i = 0; i < 999; i += 2) {
        ASSERT_TRUE(edge(vertex(i,g), vertex(i+1, g), g).second);
    }
    ASSERT_EQ(num_vertices(g), 1000);
    ASSERT_EQ(num_edges(g), 500);
}

//cyclic graph, adjacent vertices
TYPED_TEST(GraphFixture, test33) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;
    ASSERT_EQ(*b, vdB);
    ++b;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdB, g);
    b = p.first;
    e = p.second;
    ASSERT_EQ(*b, vdC);
    ++b;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdC, g);
    b = p.first;
    e = p.second;
    ASSERT_EQ(*b, vdA);
    ++b;
    ASSERT_EQ(b, e);
}

//add_edges
TYPED_TEST(GraphFixture, test34) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;
    ASSERT_EQ(num_vertices(g), 0);
    ASSERT_EQ(num_edges(g), 0);
    for(int i = 10; i < 100; ++i) {
        add_edge(i, i+1, g);
    }
    ASSERT_EQ(num_edges(g), 90);
    ASSERT_EQ(num_vertices(g), 101);
}

//add edges to non-existent vertices
TYPED_TEST(GraphFixture, test35) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    ASSERT_EQ(num_vertices(g), 0);
    ASSERT_EQ(num_edges(g), 0);
    for(int i = 10; i < 100; i+=2) {
        add_edge(i, i+1, g);
    }
    for(int i = 10; i < 100; ++i) {
        add_edge(i-1, i, g);
    }
    ASSERT_EQ(num_edges(g), 90);
    ASSERT_EQ(num_vertices(g), 100);
}

//add edges to non-existent vertices
TYPED_TEST(GraphFixture, test36) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    for(int i = 0; i < 100; ++i) {
        add_edge(i, i+1, g);
    }
    ASSERT_EQ(num_vertices(g), 101);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 104);
    add_edge(vertex(103, g), vertex(104,g), g);
    ASSERT_EQ(num_vertices(g), 105);
}

//add loops to non-existent vertices
TYPED_TEST(GraphFixture, test37) {
    using graph_type        = typename TestFixture::graph_type;
    graph_type g;
    for(int i = 0; i < 100; ++i) {
        add_edge(i, i, g);
    }
    ASSERT_EQ(num_vertices(g), 100);
    ASSERT_EQ(num_edges(g), 100);
}

// multiple graphs, testing vertex indices
TYPED_TEST(GraphFixture, test38) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    graph_type g2;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g2);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g2);
    ASSERT_EQ(v1,v2);
    ASSERT_EQ(v3,v4);
    ASSERT_EQ(num_vertices(g), num_vertices(g2));
}

// multiple graphs, testing vertex indices and edges
TYPED_TEST(GraphFixture, test39) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    graph_type g2;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g2);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g2);

    ASSERT_TRUE(add_edge(v1,v3,g).second);
    ASSERT_FALSE(add_edge(v2,v4,g).second);
    ASSERT_TRUE(add_edge(v2,v4,g2).second);
    ASSERT_FALSE(add_edge(v1,v3,g2).second);
    ASSERT_EQ(num_vertices(g), num_vertices(g2));
    ASSERT_EQ(num_edges(g), num_edges(g2));
}