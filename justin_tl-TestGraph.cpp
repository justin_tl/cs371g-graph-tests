// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
    graph_types =
    Types<
            boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
          Graph // uncomment, add a comma to the line above
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);}

    TYPED_TEST(GraphFixture, add_edge1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    graph_type g;

    vertex_descriptor vd1 = add_vertex(g);
    vertex_descriptor vd2 = add_vertex(g);
    pair<edge_descriptor, bool> answer = add_edge(vd1, vd2, g);
    ASSERT_TRUE(answer.second);
}
    TYPED_TEST(GraphFixture, add_edge2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vd1 = add_vertex(g);
    vertex_descriptor vd2 = add_vertex(g);
    pair<edge_descriptor, bool> answer = add_edge(vd1, vd2, g);
    pair<edge_descriptor, bool> answer2 = add_edge(vd2, vd1, g);

    ASSERT_TRUE(answer.second);
    ASSERT_TRUE(answer2.second);
}

    TYPED_TEST(GraphFixture, add_edge3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vd1 = add_vertex(g);
    vertex_descriptor vd2 = add_vertex(g);
    pair<edge_descriptor, bool> answer = add_edge(vd1, vd2, g);
    pair<edge_descriptor, bool> answer2 = add_edge(vd2, vd1, g);
    pair<edge_descriptor, bool> answer3 = add_edge(vd2, vd1, g);
    ASSERT_TRUE(answer.second);
    ASSERT_TRUE(answer2.second);
    ASSERT_FALSE(answer3.second);
}
    TYPED_TEST(GraphFixture, add_edge4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vd1 = add_vertex(g);
    vertex_descriptor vd2 = add_vertex(g);
    pair<edge_descriptor, bool> answer = add_edge(vd1, vd2, g);
    pair<edge_descriptor, bool> answer2 = add_edge(vd2, vd1, g);
    pair<edge_descriptor, bool> answer3 = add_edge(vd2, vd1, g);
    pair<edge_descriptor, bool> answer4 = add_edge(vd1, vd2, g);
    ASSERT_TRUE(answer.second);
    ASSERT_TRUE(answer2.second);
    ASSERT_FALSE(answer3.second);
    ASSERT_FALSE(answer4.second);
}
TYPED_TEST(GraphFixture, add_vertex1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 1);
}
TYPED_TEST(GraphFixture, add_vertex2) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 2);
}
TYPED_TEST(GraphFixture, add_vertex3) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 3);
}
TYPED_TEST(GraphFixture, add_vertex4) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 4);
}
TYPED_TEST(GraphFixture, add_vertex5) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 5);
}
TYPED_TEST(GraphFixture, add_vertex6) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 6);
}
TYPED_TEST(GraphFixture, add_vertex7) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 7);
}
TYPED_TEST(GraphFixture, add_vertex8) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 8);
}
TYPED_TEST(GraphFixture, add_vertex9) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 9);
}
TYPED_TEST(GraphFixture, add_vertex10) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 10);
}
TYPED_TEST(GraphFixture, add_vertex11) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 11);
}
TYPED_TEST(GraphFixture, add_vertex12) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 12);
}
TYPED_TEST(GraphFixture, add_vertex13) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 13);
}
TYPED_TEST(GraphFixture, add_vertex14) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 14);
}
TYPED_TEST(GraphFixture, add_vertex15) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 15);
}
TYPED_TEST(GraphFixture, add_vertex16) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 16);
}
TYPED_TEST(GraphFixture, add_vertex17) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 17);
}
TYPED_TEST(GraphFixture, add_vertex18) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 18);
}
TYPED_TEST(GraphFixture, add_vertex19) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 19);
}
TYPED_TEST(GraphFixture, add_vertex20) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 20);
}
TYPED_TEST(GraphFixture, add_vertex21) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 21);
}
TYPED_TEST(GraphFixture, add_vertex22) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 22);
}
TYPED_TEST(GraphFixture, add_vertex23) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 23);
}
TYPED_TEST(GraphFixture, add_vertex24) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 24);
}
TYPED_TEST(GraphFixture, add_vertex25) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 25);
}
TYPED_TEST(GraphFixture, add_vertex26) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 26);
}
TYPED_TEST(GraphFixture, add_vertex27) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 27);
}
TYPED_TEST(GraphFixture, add_vertex28) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 28);
}
TYPED_TEST(GraphFixture, add_vertex29) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 29);
}
TYPED_TEST(GraphFixture, add_vertex30) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 30);
}
TYPED_TEST(GraphFixture, adjacent_vertices1)
{

using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vd1 = add_vertex(g);
    vertex_descriptor vd2 = add_vertex(g);
    add_edge(vd1, vd2, g);
    pair<adjacency_iterator, adjacency_iterator> ad = adjacent_vertices(vd1, g);
    adjacency_iterator b = ad.first;
    adjacency_iterator e = ad.second;
    ASSERT_NE(b, e);

}
TYPED_TEST(GraphFixture, edge1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vd1 = add_vertex(g);
    vertex_descriptor vd2 = add_vertex(g);


    pair<edge_descriptor, bool> ed = edge(vd1, vd2, g);



    ASSERT_FALSE(ed.second);
}
TYPED_TEST(GraphFixture, edges1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;


    add_vertex(g);
    add_vertex(g);

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;

    ASSERT_EQ(b, e);

}
TYPED_TEST(GraphFixture, num_edges1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;


    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_edges(g), 0);

}
TYPED_TEST(GraphFixture, num_vertices1)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 2);
}
TYPED_TEST(GraphFixture, source1)
{
	using graph_type 		= typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
	using edge_descriptor 	= typename TestFixture::edge_descriptor;

    graph_type g;

	vertex_descriptor vd1 = add_vertex(g);
	vertex_descriptor vd2 = add_vertex(g);

	pair<edge_descriptor, bool> p = add_edge(vd1, vd2, g);

	vertex_descriptor s = source(p.first, g);

	ASSERT_EQ(s, vd1);

}
TYPED_TEST(GraphFixture, target1)
{
	using graph_type 		= typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
	using edge_descriptor 	= typename TestFixture::edge_descriptor;

    graph_type g;

	vertex_descriptor vd1 = add_vertex(g);
	vertex_descriptor vd2 = add_vertex(g);

	pair<edge_descriptor, bool> p = add_edge(vd1, vd2, g);

	vertex_descriptor t = target(p.first, g);

	ASSERT_EQ(t, vd2);
}

TYPED_TEST(GraphFixture, vertex1)
{
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor v = add_vertex(g);
    ASSERT_EQ(v, vertex(0, g));
    
}
TYPED_TEST(GraphFixture, vertices1)
{
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    graph_type g;
    add_vertex(g);
       pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;

    ASSERT_NE(b, e);
    
}




