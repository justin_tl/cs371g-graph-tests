// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
    graph_types =
    Types<
            boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
            Graph 
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);}

//----------
// add edge
//----------

TYPED_TEST(GraphFixture, test5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    ASSERT_EQ(num_edges(g)    , 0);
    ASSERT_EQ(num_vertices(g) , 5);
    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdE, g);

    ASSERT_EQ(num_edges(g)    , 5);
    ASSERT_EQ(num_vertices(g) , 5);
}

TYPED_TEST(GraphFixture, test6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vdA = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 1);
    vertex_descriptor vdB = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 2);
    
    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);

    ASSERT_EQ(num_edges(g)    , 2);
    ASSERT_EQ(num_vertices(g) , 2);

    // Edge A B already exist, can't add anymore
    add_edge(vdA, vdB, g); 
    ASSERT_EQ(num_edges(g)    , 2);
    
    // create an edge from A to A
    add_edge(vdA, vdA, g); 
    ASSERT_EQ(num_edges(g)    , 3);
    // can't do multiple edges to yourself
    add_edge(vdA, vdA, g); 
    ASSERT_EQ(num_edges(g)    , 3);
}

TYPED_TEST(GraphFixture, test7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    for (int i = 0; i < 10; ++i){
        vertex_descriptor vdA = add_vertex(g);
        vertex_descriptor vdB = add_vertex(g);
        add_edge(vdA, vdB, g); 
    }
    // vertex AB is making a new one each time
    ASSERT_EQ(num_edges(g) , 10);
}

TYPED_TEST(GraphFixture, test8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    ASSERT_EQ(num_edges(g)    , 0);
    ASSERT_EQ(num_vertices(g) , 5);
    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdE, g);

    ASSERT_EQ(num_edges(g)    , 5);
    ASSERT_EQ(num_vertices(g) , 5);

    graph_type t;
    t = g;
    ASSERT_EQ(num_edges(g)    , 5);
    ASSERT_EQ(num_vertices(g) , 5);
    ASSERT_EQ(num_edges(t)    , 5);
    ASSERT_EQ(num_vertices(t) , 5);
    
    // adding new edges don't change g
    vertex_descriptor vdF = add_vertex(t);
    vertex_descriptor vdG = add_vertex(t);
    add_edge(vdA, vdF, t);
    add_edge(vdA, vdG, t);
    ASSERT_EQ(num_edges(g)    , 5);
    ASSERT_EQ(num_vertices(g) , 5);
    ASSERT_EQ(num_edges(t)    , 7);
    ASSERT_EQ(num_vertices(t) , 7);
}

//----------
// add vertex
//----------

TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vdA = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 1);
    vertex_descriptor vdB = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 2);
    vertex_descriptor vdC = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 3);
    vertex_descriptor vdD = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 4);
    vertex_descriptor vdE = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 5);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdE, g);
}

TYPED_TEST(GraphFixture, test10) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    for (int i = 0; i < 10; ++i){
        vertex_descriptor vdA = add_vertex(g);
        add_edge(vdA, vdA, g);
    }
    
    // only make one because of the loop
    ASSERT_EQ(num_vertices(g) , 10);
    ASSERT_EQ(num_edges(g)    , 10);
}

TYPED_TEST(GraphFixture, test11) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vdA = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 1);
    vertex_descriptor vdB = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 2);
    vertex_descriptor vdC = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 3);
    vertex_descriptor vdD = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 4);
    vertex_descriptor vdE = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 5);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdE, g);

    // new graph got assigned from g
    graph_type t;
    t = g;
    ASSERT_EQ(num_vertices(g) , 5);
    ASSERT_EQ(num_vertices(t) , 5);
}
//----------
// add edge
//----------

TYPED_TEST(GraphFixture, test12) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vdA = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 1);
    vertex_descriptor vdB = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 2);
    vertex_descriptor vdC = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 3);
    vertex_descriptor vdD = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 4);
    vertex_descriptor vdE = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 5);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdE, g);

    // new graph got assigned from g
    graph_type t;
    t = g;
    ASSERT_EQ(num_vertices(g) , 5);
    ASSERT_EQ(num_vertices(t) , 5);
    
    // adding new vertices to t doesn't change g
    vertex_descriptor vdF = add_vertex(t);
    vertex_descriptor vdG = add_vertex(t);
    ASSERT_EQ(num_vertices(g) , 5);
    ASSERT_EQ(num_vertices(t) , 7);
}

//----------
// adjacent_vertices
//----------
TYPED_TEST(GraphFixture, test13) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(vdA, vdB, g);
    pair<adjacency_iterator, adjacency_iterator> ad = adjacent_vertices(vdA, g);
    adjacency_iterator b = ad.first;
    adjacency_iterator e = ad.second;
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test14) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> eB = add_edge(vdA, vdC, g);
    pair<adjacency_iterator, adjacency_iterator> ad = adjacent_vertices(vdA, g);
    adjacency_iterator b = ad.first;
    adjacency_iterator e = ad.second;
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, vdB);
    ++b;
    ASSERT_EQ(*b, vdC);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test15) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> eB = add_edge(vdA, vdC, g);
    pair<adjacency_iterator, adjacency_iterator> ad = adjacent_vertices(vdA, g);
    adjacency_iterator b = ad.first;
    adjacency_iterator e = ad.second;
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, vdB);
    ++b;
    ASSERT_EQ(*b, vdC);
    ++b;
    ASSERT_EQ(b, e);
    // adding a new edge won't change ad
    pair<edge_descriptor, bool> eC = add_edge(vdA, vdD, g);
    ASSERT_EQ(b, e);
}

//----------
// edge
//----------
TYPED_TEST(GraphFixture, test16) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> test = edge(vdA, vdB, g);

    ASSERT_EQ(test, eA);
}

TYPED_TEST(GraphFixture, test17) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> eB = add_edge(vdB, vdA, g);
    pair<edge_descriptor, bool> test = edge(vdA, vdB, g);
    pair<edge_descriptor, bool> test2 = edge(vdB, vdA, g);

    ASSERT_NE(test2, test);
    ASSERT_EQ(test, eA);
    ASSERT_EQ(test2, eB);
}

//----------
// edges
//----------
TYPED_TEST(GraphFixture, test18) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(vdA, vdB, g);
    pair<edge_iterator, edge_iterator> ed = edges(g);
    edge_iterator b = ed.first;
    edge_iterator e = ed.second;
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, eA.first);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test19) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> eB = add_edge(vdB, vdA, g);
    pair<edge_iterator, edge_iterator> ed = edges(g);
    edge_iterator b = ed.first;
    edge_iterator e = ed.second;
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, eA.first);
    ++b;
    ASSERT_EQ(*b, eB.first);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test20) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    pair<edge_iterator, edge_iterator> ed = edges(g);
    edge_iterator b = ed.first;
    edge_iterator e = ed.second;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test21) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> eB = add_edge(vdA, vdB, g);
    pair<edge_iterator, edge_iterator> ed = edges(g);
    edge_iterator b = ed.first;
    edge_iterator e = ed.second;
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, eA.first);
    ASSERT_EQ(*b, eB.first);
    ++b;
    ASSERT_EQ(b, e);
}

//----------
// num edges
//----------
TYPED_TEST(GraphFixture, test22) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, test23) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(vdA, vdB, g);
    ASSERT_EQ(num_edges(g), 1);
    pair<edge_descriptor, bool> eB = add_edge(vdB, vdA, g);
    ASSERT_EQ(num_edges(g), 2);
}

TYPED_TEST(GraphFixture, test24) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(vdA, vdB, g);
    ASSERT_EQ(num_edges(g), 1);
    pair<edge_descriptor, bool> eB = add_edge(vdA, vdB, g);
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, test25) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(add_vertex(g), add_vertex(g), g);
    ASSERT_EQ(num_edges(g), 1);
    pair<edge_descriptor, bool> eB = add_edge(add_vertex(g), add_vertex(g), g);
    ASSERT_EQ(num_edges(g), 2);
}

//----------
// num vertices
//----------

TYPED_TEST(GraphFixture, test26) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;
    ASSERT_EQ(num_vertices(g), 0);
}

TYPED_TEST(GraphFixture, test27) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    ASSERT_EQ(num_vertices(g), 2);
}

TYPED_TEST(GraphFixture, test28) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> eA = add_edge(add_vertex(g), add_vertex(g), g);
    ASSERT_EQ(num_vertices(g), 4);
    pair<edge_descriptor, bool> eB = add_edge(add_vertex(g), add_vertex(g), g);
    ASSERT_EQ(num_vertices(g), 6);
}

//----------
// source
//----------
TYPED_TEST(GraphFixture, test29) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    pair<edge_descriptor, bool> eB = add_edge(vA, vB, g);
    // check if bool is true or false first
    vertex_descriptor vT = source(eB.first, g);
    ASSERT_EQ(vT , vA);
}

TYPED_TEST(GraphFixture, test30) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;
    pair<edge_descriptor, bool> eB = add_edge(add_vertex(g), add_vertex(g), g);
    // check if bool is true or false first
    vertex_descriptor vT = source(eB.first, g);
    // the source is the first add
    ASSERT_EQ(vT , 1);
}


TYPED_TEST(GraphFixture, test31) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    std::pair<edge_descriptor, bool> eA = add_edge(vA, vB, g);
    std::pair<edge_descriptor, bool> eB = add_edge(vB, vC, g);
    std::pair<edge_descriptor, bool> eC = add_edge(vC, vA, g);
    // check if bool is true or false first
    vertex_descriptor vT  = source(eA.first, g);
    vertex_descriptor vT1 = source(eB.first, g);
    vertex_descriptor vT2 = source(eC.first, g);
    // the source is the first add
    ASSERT_EQ(vT  , vA);
    ASSERT_EQ(vT1 , vB);
    ASSERT_EQ(vT2 , vC);
}

TYPED_TEST(GraphFixture, test32) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    // add back and forth edge
    std::pair<edge_descriptor, bool> eA = add_edge(vA, vB, g);
    std::pair<edge_descriptor, bool> eB = add_edge(vB, vA, g);
    vertex_descriptor vT  = source(eA.first, g);
    vertex_descriptor vT1 = source(eB.first, g);
    // the source is the first add
    ASSERT_EQ(vT  , vA);
    ASSERT_EQ(vT1 , vB);
}


//----------
// target
//----------
TYPED_TEST(GraphFixture, test33) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    std::pair<edge_descriptor, bool> eB = add_edge(vA, vB, g);
    // check if bool is true or false first
    vertex_descriptor vT = target(eB.first, g);
    ASSERT_EQ(vT , vB);
}

TYPED_TEST(GraphFixture, test34) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    add_edge(vA, vC, g); // already add the edge
    std::pair<edge_descriptor, bool> eC = add_edge(vA, vC, g); // this is false
    ASSERT_FALSE(eC.second);
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, test35) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    // adding to index 2
    std::pair<edge_descriptor, bool> eC = add_edge(vA, add_vertex(g), g);
    vertex_descriptor vT = target(eC.first, g);
    ASSERT_EQ(vT, 2);
    ASSERT_TRUE(eC.second);
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, test36) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    //vertex_descriptor vA = add_vertex(g);
    // second add excute first
    pair<edge_descriptor, bool> eB = add_edge( add_vertex(g) , add_vertex(g), g);
    pair<edge_descriptor, bool> eC = add_edge( add_vertex(g), add_vertex(g), g);
    vertex_descriptor vS1 = source(eB.first, g);
    vertex_descriptor vT1 = target(eB.first, g);
    vertex_descriptor vT2 = target(eC.first, g);
    vertex_descriptor vS2 = source(eC.first, g);
    ASSERT_EQ(vT1, 0);
    ASSERT_EQ(vS2, 3);
    ASSERT_TRUE(eC.second);
    ASSERT_EQ(num_edges(g), 2);
}


//----------
// vertex
//----------
TYPED_TEST(GraphFixture, test37) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vD = add_vertex(g);
    
    vertex_descriptor vC = vertex(0, g);
    ASSERT_EQ(num_vertices(g) , 3);
    ASSERT_EQ(vC , vA);
    
}

TYPED_TEST(GraphFixture, test38) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    vertex_descriptor vD = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 4);
    vertex_descriptor vE = vertex(3, g);
    ASSERT_EQ(vD , vE);
    
}

TYPED_TEST(GraphFixture, test39) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

     ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vdA = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 1);
    vertex_descriptor vdB = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 2);

    // last one is num_vetices - 1
    vertex_descriptor vTemp = vertex(1, g);
    ASSERT_EQ(vTemp , vdB);

    // making a new vertex will make a new one at that index
    vertex_descriptor vTemp2 = vertex(10, g);
    vertex_descriptor vTemp3 = vertex(10, g);
    ASSERT_EQ(vTemp2 , vTemp3);
    ASSERT_EQ(num_vertices(g) , 2); // we still have have 3
}

TYPED_TEST(GraphFixture, test40) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vdA = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 1);
    vertex_descriptor vdB = add_vertex(g);
    ASSERT_EQ(num_vertices(g) , 2);

    // last one is num_vetices - 1
    vertex_descriptor vTemp = vertex(1, g);
    ASSERT_EQ(vTemp , vdB);

    // is not making anew new vertex
    vertex_descriptor vTemp2 = vertex(4, g);
    vertex_descriptor vTemp3 = vertex(4, g);
    ASSERT_EQ(vTemp2 , vTemp3);
    ASSERT_EQ(num_vertices(g) , 2); // we still have have 2 (A,B)

    // continue adding
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g); //index 4
    ASSERT_EQ(num_vertices(g) , 5); // this should change?? A B C D E vTEmp2??

    vertex_descriptor vTemp4 = vertex(4, g);
    ASSERT_EQ(vTemp4 , vTemp2);
    ASSERT_EQ(vTemp4 , vdE);
    ASSERT_EQ(vTemp2 , vdE); // just comparing the index
    // so equal of two decriptor just compare index??
}

//----------
// vertices
//----------
TYPED_TEST(GraphFixture, test41) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    
    graph_type g;
    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    vertex_descriptor vD = add_vertex(g);
    std::pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;
    vertex_iterator temp = b;
    ++++b;
    ++++b;
    ASSERT_EQ(b, e);
    ----e;
    ----e;
    ASSERT_EQ(e, temp);
}

TYPED_TEST(GraphFixture, test42) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    
    graph_type g;
    // empty case
    ASSERT_EQ(num_vertices(g) , 0);
    std::pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test43) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    
    graph_type g;
    
    ASSERT_EQ(num_vertices(g) , 0);
    vertex_descriptor vA = add_vertex(g);
    vertex_descriptor vB = add_vertex(g);
    vertex_descriptor vC = add_vertex(g);
    vertex_descriptor vD = add_vertex(g);
    std::pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;
    // test iteration
    vertex_descriptor temp = *b;
    ASSERT_EQ(temp, vA);
    b++;
    temp = *b;
    ASSERT_EQ(temp, vB);
    b++;
    temp = *b;
    ASSERT_EQ(temp, vC);
    b++;
    temp = *b;
    ASSERT_EQ(temp, vD);
    b++;
    ASSERT_EQ(b, e);
}