// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream>  // cout, endl
#include <iterator>  // ostream_iterator
#include <sstream>   // ostringstream
#include <utility>   // pair

#include "Graph.hpp"
#include "boost/graph/adjacency_list.hpp"  // adjacency_list
#include "gtest/gtest.h"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test
{
  using graph_type = T;
  using vertex_descriptor = typename graph_type::vertex_descriptor;
  using edge_descriptor = typename graph_type::edge_descriptor;
  using vertex_iterator = typename graph_type::vertex_iterator;
  using edge_iterator = typename graph_type::edge_iterator;
  using adjacency_iterator = typename graph_type::adjacency_iterator;
  using vertices_size_type = typename graph_type::vertices_size_type;
  using edges_size_type = typename graph_type::edges_size_type;

  // basic graphs
  //  empty, dag, cycle, fake undirected, star, fully connected, completely sparse
  static graph_type empty_graph()
  {
    return graph_type();
  }

  static graph_type dag()
  {
    graph_type g;
    vertex_descriptor vd_a = add_vertex(g);
    vertex_descriptor vd_b = add_vertex(g);
    vertex_descriptor vd_c = add_vertex(g);
    vertex_descriptor vd_d = add_vertex(g);
    vertex_descriptor vd_e = add_vertex(g);
    vertex_descriptor vd_f = add_vertex(g);

    add_edge(vd_a, vd_b, g);
    add_edge(vd_c, vd_b, g);
    add_edge(vd_b, vd_d, g);
    add_edge(vd_d, vd_e, g);
    add_edge(vd_d, vd_f, g);
    return g;
  }

  static graph_type fake_undirected()
  {
    graph_type g;
    vertex_descriptor vd_a = add_vertex(g);
    vertex_descriptor vd_b = add_vertex(g);
    vertex_descriptor vd_c = add_vertex(g);
    vertex_descriptor vd_d = add_vertex(g);
    vertex_descriptor vd_e = add_vertex(g);
    vertex_descriptor vd_f = add_vertex(g);

    add_edge(vd_a, vd_b, g);
    add_edge(vd_c, vd_b, g);
    add_edge(vd_b, vd_d, g);
    add_edge(vd_d, vd_e, g);
    add_edge(vd_d, vd_f, g);

    add_edge(vd_b, vd_a, g);
    add_edge(vd_b, vd_c, g);
    add_edge(vd_d, vd_b, g);
    add_edge(vd_e, vd_d, g);
    add_edge(vd_f, vd_d, g);
    return g;
  }

  static graph_type cycle()
  {
    graph_type g;
    vertex_descriptor vd_a = add_vertex(g);
    vertex_descriptor vd_b = add_vertex(g);
    vertex_descriptor vd_c = add_vertex(g);
    vertex_descriptor vd_d = add_vertex(g);
    vertex_descriptor vd_e = add_vertex(g);
    vertex_descriptor vd_f = add_vertex(g);

    add_edge(vd_a, vd_b, g);
    add_edge(vd_c, vd_b, g);
    add_edge(vd_b, vd_d, g);
    add_edge(vd_d, vd_e, g);
    add_edge(vd_d, vd_f, g);

    // Connect end to beginning
    add_edge(vd_f, vd_a, g);
    return g;
  }

  static graph_type self_cycle()
  {
    graph_type g;
    vertex_descriptor vd_a = add_vertex(g);
    vertex_descriptor vd_b = add_vertex(g);

    add_edge(vd_a, vd_b, g);
    add_edge(vd_a, vd_a, g);
    return g;
  }
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using graph_types = Types<boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>, Graph>;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

// Tests 1-5: Provided Tests

TYPED_TEST(GraphFixture, test0)
{
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using vertices_size_type = typename TestFixture::vertices_size_type;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);

  vertex_descriptor vd = vertex(0, g);
  ASSERT_EQ(vd, vdA);

  vertices_size_type vs = num_vertices(g);
  ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1)
{
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;
  using edges_size_type = typename TestFixture::edges_size_type;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  edge_descriptor edAB = add_edge(vdA, vdB, g).first;

  pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
  ASSERT_EQ(p1.first, edAB);
  ASSERT_EQ(p1.second, false);

  pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
  ASSERT_EQ(p2.first, edAB);
  ASSERT_EQ(p2.second, true);

  edges_size_type es = num_edges(g);
  ASSERT_EQ(es, 1u);

  vertex_descriptor vd1 = source(edAB, g);
  ASSERT_EQ(vd1, vdA);

  vertex_descriptor vd2 = target(edAB, g);
  ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2)
{
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using vertex_iterator = typename TestFixture::vertex_iterator;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  pair<vertex_iterator, vertex_iterator> p = vertices(g);
  vertex_iterator b = p.first;
  vertex_iterator e = p.second;
  ASSERT_NE(b, e);

  vertex_descriptor vd1 = *b;
  ASSERT_EQ(vd1, vdA);
  ++b;
  ASSERT_NE(b, e);

  vertex_descriptor vd2 = *b;
  ASSERT_EQ(vd2, vdB);
  ++b;
  ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3)
{
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;
  using edge_iterator = typename TestFixture::edge_iterator;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  edge_descriptor edAB = add_edge(vdA, vdB, g).first;
  edge_descriptor edAC = add_edge(vdA, vdC, g).first;

  pair<edge_iterator, edge_iterator> p = edges(g);
  edge_iterator b = p.first;
  edge_iterator e = p.second;
  ASSERT_NE(b, e);

  edge_descriptor ed1 = *b;
  ASSERT_EQ(ed1, edAB);
  ++b;
  ASSERT_NE(b, e);

  edge_descriptor ed2 = *b;
  ASSERT_EQ(ed2, edAC);
  ++b;
  ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4)
{
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using adjacency_iterator = typename TestFixture::adjacency_iterator;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  add_edge(vdA, vdC, g);

  pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
  adjacency_iterator b = p.first;
  adjacency_iterator e = p.second;
  ASSERT_NE(b, e);

  vertex_descriptor vd1 = *b;
  ASSERT_EQ(vd1, vdB);
  ++b;
  ASSERT_NE(b, e);

  vertex_descriptor vd2 = *b;
  ASSERT_EQ(vd2, vdC);
  ++b;
  ASSERT_EQ(e, b);
}

// Tests 6-9: vertex tests

TYPED_TEST(GraphFixture, vertex1)
{
  auto g = TestFixture::dag();
  ASSERT_EQ(vertex(0, g), 0);
  ASSERT_EQ(vertex(1, g), 1);
  ASSERT_EQ(vertex(2, g), 2);
  ASSERT_EQ(vertex(3, g), 3);
  ASSERT_EQ(vertex(4, g), 4);
  ASSERT_EQ(vertex(5, g), 5);
}

TYPED_TEST(GraphFixture, vertex2)
{
  auto g = TestFixture::fake_undirected();
  ASSERT_EQ(vertex(0, g), 0);
  ASSERT_EQ(vertex(1, g), 1);
  ASSERT_EQ(vertex(2, g), 2);
  ASSERT_EQ(vertex(3, g), 3);
  ASSERT_EQ(vertex(4, g), 4);
  ASSERT_EQ(vertex(5, g), 5);
}

TYPED_TEST(GraphFixture, vertex3)
{
  auto g = TestFixture::cycle();
  ASSERT_EQ(vertex(0, g), 0);
  ASSERT_EQ(vertex(1, g), 1);
  ASSERT_EQ(vertex(2, g), 2);
  ASSERT_EQ(vertex(3, g), 3);
  ASSERT_EQ(vertex(4, g), 4);
  ASSERT_EQ(vertex(5, g), 5);
}

TYPED_TEST(GraphFixture, vertex4)
{
  auto g = TestFixture::self_cycle();
  ASSERT_EQ(vertex(0, g), 0);
  ASSERT_EQ(vertex(1, g), 1);
}

// Tests 10-13: edge tests

TYPED_TEST(GraphFixture, edge1)
{
  auto g = TestFixture::dag();
  ASSERT_EQ(edge(0, 1, g).second, true);
  ASSERT_EQ(edge(2, 1, g).second, true);
  ASSERT_EQ(edge(1, 3, g).second, true);
  ASSERT_EQ(edge(3, 4, g).second, true);
  ASSERT_EQ(edge(3, 5, g).second, true);
}

TYPED_TEST(GraphFixture, edge2)
{
  auto g = TestFixture::fake_undirected();
  ASSERT_EQ(edge(0, 1, g).second, true);
  ASSERT_EQ(edge(2, 1, g).second, true);
  ASSERT_EQ(edge(1, 3, g).second, true);
  ASSERT_EQ(edge(3, 4, g).second, true);
  ASSERT_EQ(edge(3, 5, g).second, true);

  ASSERT_EQ(edge(1, 0, g).second, true);
  ASSERT_EQ(edge(1, 2, g).second, true);
  ASSERT_EQ(edge(3, 1, g).second, true);
  ASSERT_EQ(edge(4, 3, g).second, true);
  ASSERT_EQ(edge(5, 3, g).second, true);
}

TYPED_TEST(GraphFixture, edge3)
{
  auto g = TestFixture::cycle();
  ASSERT_EQ(edge(0, 1, g).second, true);
  ASSERT_EQ(edge(2, 1, g).second, true);
  ASSERT_EQ(edge(1, 3, g).second, true);
  ASSERT_EQ(edge(3, 4, g).second, true);
  ASSERT_EQ(edge(3, 5, g).second, true);
  ASSERT_EQ(edge(5, 0, g).second, true);
}

TYPED_TEST(GraphFixture, edge4)
{
  auto g = TestFixture::self_cycle();
  ASSERT_EQ(edge(0, 1, g).second, true);
  ASSERT_EQ(edge(0, 0, g).second, true);
}

// Tests 14-18: add_vertex tests

TYPED_TEST(GraphFixture, add_vertex0)
{
  auto g = TestFixture::empty_graph();
  typename TestFixture::vertex_descriptor v = add_vertex(g);
  ASSERT_EQ(v, vertex(0, g));
}

TYPED_TEST(GraphFixture, add_vertex1)
{
  auto g = TestFixture::dag();
  typename TestFixture::vertex_descriptor v1 = add_vertex(g);
  typename TestFixture::vertex_descriptor v2 = add_vertex(g);
  ASSERT_EQ(v1, vertex(6, g));
  ASSERT_EQ(v2, vertex(7, g));
}

TYPED_TEST(GraphFixture, add_vertex2)
{
  auto g = TestFixture::fake_undirected();
  typename TestFixture::vertex_descriptor v = add_vertex(g);
  ASSERT_EQ(v, vertex(6, g));
}

TYPED_TEST(GraphFixture, add_vertex3)
{
  auto g = TestFixture::cycle();
  typename TestFixture::vertex_descriptor v = add_vertex(g);
  ASSERT_EQ(v, vertex(6, g));
}

TYPED_TEST(GraphFixture, add_vertex4)
{
  auto g = TestFixture::self_cycle();
  typename TestFixture::vertex_descriptor v = add_vertex(g);
  ASSERT_EQ(v, vertex(2, g));
}

// Tests 19-22: add_edge tests

TYPED_TEST(GraphFixture, add_edge1)
{
  auto g = TestFixture::dag();
  ASSERT_EQ(edge(0, 2, g).second, false);
  add_edge(0, 2, g);
  ASSERT_EQ(edge(0, 2, g).second, true);

  ASSERT_EQ(edge(1, 5, g).second, false);
  add_edge(1, 5, g);
  ASSERT_EQ(edge(1, 5, g).second, true);
}

TYPED_TEST(GraphFixture, add_edge2)
{
  auto g = TestFixture::fake_undirected();
  ASSERT_EQ(edge(0, 2, g).second, false);
  add_edge(0, 2, g);
  ASSERT_EQ(edge(0, 2, g).second, true);

  ASSERT_EQ(edge(1, 5, g).second, false);
  add_edge(1, 5, g);
  ASSERT_EQ(edge(1, 5, g).second, true);
}

TYPED_TEST(GraphFixture, add_edge3)
{
  auto g = TestFixture::cycle();
  ASSERT_EQ(edge(0, 2, g).second, false);
  add_edge(0, 2, g);
  ASSERT_EQ(edge(0, 2, g).second, true);

  ASSERT_EQ(edge(1, 5, g).second, false);
  add_edge(1, 5, g);
  ASSERT_EQ(edge(1, 5, g).second, true);
}

TYPED_TEST(GraphFixture, add_edge4)
{
  auto g = TestFixture::self_cycle();
  ASSERT_EQ(edge(1, 0, g).second, false);
  add_edge(1, 0, g);
  ASSERT_EQ(edge(1, 0, g).second, true);
}

// Tests 23-27: num_vertices tests

TYPED_TEST(GraphFixture, num_vert0)
{
  auto g = TestFixture::empty_graph();
  ASSERT_EQ(num_vertices(g), 0);
  add_vertex(g);
  ASSERT_EQ(num_vertices(g), 1);
}

TYPED_TEST(GraphFixture, num_vert1)
{
  auto g = TestFixture::dag();
  ASSERT_EQ(num_vertices(g), 6);
  add_vertex(g);
  ASSERT_EQ(num_vertices(g), 7);
}

TYPED_TEST(GraphFixture, num_vert2)
{
  auto g = TestFixture::fake_undirected();
  ASSERT_EQ(num_vertices(g), 6);
  add_vertex(g);
  ASSERT_EQ(num_vertices(g), 7);
}

TYPED_TEST(GraphFixture, num_vert3)
{
  auto g = TestFixture::cycle();
  ASSERT_EQ(num_vertices(g), 6);
  add_vertex(g);
  ASSERT_EQ(num_vertices(g), 7);
}

TYPED_TEST(GraphFixture, num_vert4)
{
  auto g = TestFixture::self_cycle();
  ASSERT_EQ(num_vertices(g), 2);
  add_vertex(g);
  ASSERT_EQ(num_vertices(g), 3);
}

// Tests 28-31: num_edges tests

TYPED_TEST(GraphFixture, num_edge1)
{
  auto g = TestFixture::dag();
  ASSERT_EQ(num_edges(g), 5);
  add_edge(3, 0, g);
  ASSERT_EQ(num_edges(g), 6);
}

TYPED_TEST(GraphFixture, num_edge2)
{
  auto g = TestFixture::fake_undirected();
  ASSERT_EQ(num_edges(g), 10);
  add_edge(3, 0, g);
  ASSERT_EQ(num_edges(g), 11);
}

TYPED_TEST(GraphFixture, num_edge3)
{
  auto g = TestFixture::cycle();
  ASSERT_EQ(num_edges(g), 6);
  add_edge(3, 0, g);
  ASSERT_EQ(num_edges(g), 7);
}

TYPED_TEST(GraphFixture, num_edge4)
{
  auto g = TestFixture::self_cycle();
  ASSERT_EQ(num_edges(g), 2);
  add_edge(1, 1, g);
  ASSERT_EQ(num_edges(g), 3);
}

TYPED_TEST(GraphFixture, vertices_0)
{
  using graph_type = typename TestFixture::graph_type;

  graph_type g = TestFixture::empty_graph();
  auto v = vertices(g);
  ASSERT_EQ(v.first, v.second);
}

TYPED_TEST(GraphFixture, vertices_1)
{
  using graph_type = typename TestFixture::graph_type;

  graph_type g = TestFixture::dag();
  auto v = vertices(g);
  ASSERT_NE(v.first, v.second);
  ASSERT_EQ(std::distance(v.first, v.second), 6);
  ASSERT_EQ(*(v.first++), vertex(0, g));
  ASSERT_EQ(*(v.first++), vertex(1, g));
  ASSERT_EQ(*(v.first++), vertex(2, g));
  ASSERT_EQ(*(v.first++), vertex(3, g));
  ASSERT_EQ(*(v.first++), vertex(4, g));
  ASSERT_EQ(*(v.first++), vertex(5, g));
  ASSERT_EQ(v.first, v.second);
}

TYPED_TEST(GraphFixture, vertices_2)
{
  using graph_type = typename TestFixture::graph_type;

  graph_type g = TestFixture::fake_undirected();
  auto v = vertices(g);
  ASSERT_NE(v.first, v.second);
  ASSERT_EQ(std::distance(v.first, v.second), 6);
  ASSERT_EQ(*(v.first++), vertex(0, g));
  ASSERT_EQ(*(v.first++), vertex(1, g));
  ASSERT_EQ(*(v.first++), vertex(2, g));
  ASSERT_EQ(*(v.first++), vertex(3, g));
  ASSERT_EQ(*(v.first++), vertex(4, g));
  ASSERT_EQ(*(v.first++), vertex(5, g));
  ASSERT_EQ(v.first, v.second);
}

TYPED_TEST(GraphFixture, vertices_3)
{
  using graph_type = typename TestFixture::graph_type;

  graph_type g = TestFixture::cycle();
  auto v = vertices(g);
  ASSERT_NE(v.first, v.second);
  ASSERT_EQ(std::distance(v.first, v.second), 6);
  ASSERT_EQ(*(v.first++), vertex(0, g));
  ASSERT_EQ(*(v.first++), vertex(1, g));
  ASSERT_EQ(*(v.first++), vertex(2, g));
  ASSERT_EQ(*(v.first++), vertex(3, g));
  ASSERT_EQ(*(v.first++), vertex(4, g));
  ASSERT_EQ(*(v.first++), vertex(5, g));
  ASSERT_EQ(v.first, v.second);
}

TYPED_TEST(GraphFixture, vertices_4)
{
  using graph_type = typename TestFixture::graph_type;

  graph_type g = TestFixture::self_cycle();
  auto v = vertices(g);
  ASSERT_NE(v.first, v.second);
  ASSERT_EQ(std::distance(v.first, v.second), 2);
  ASSERT_EQ(*(v.first++), vertex(0, g));
  ASSERT_EQ(*(v.first++), vertex(1, g));
  ASSERT_EQ(v.first, v.second);
}

TYPED_TEST(GraphFixture, adjacent_vertices_1)
{
  using graph_type = typename TestFixture::graph_type;

  graph_type g = TestFixture::dag();
  auto adj_0 = adjacent_vertices(vertex(0, g), g);
  ASSERT_EQ(*(adj_0.first++), vertex(1, g));
  ASSERT_EQ(adj_0.first, adj_0.second);
  auto adj_2 = adjacent_vertices(vertex(2, g), g);
  ASSERT_EQ(*(adj_2.first++), vertex(1, g));
  ASSERT_EQ(adj_2.first, adj_2.second);
  auto adj_1 = adjacent_vertices(vertex(1, g), g);
  ASSERT_EQ(*(adj_1.first++), vertex(3, g));
  ASSERT_EQ(adj_1.first, adj_1.second);
  auto adj_3 = adjacent_vertices(vertex(3, g), g);
  ASSERT_EQ(*(adj_3.first++), vertex(4, g));
  ASSERT_EQ(*(adj_3.first++), vertex(5, g));
  ASSERT_EQ(adj_3.first, adj_3.second);
  auto adj_4 = adjacent_vertices(vertex(4, g), g);
  ASSERT_EQ(adj_4.first, adj_4.second);
  auto adj_5 = adjacent_vertices(vertex(5, g), g);
  ASSERT_EQ(adj_5.first, adj_5.second);
}

TYPED_TEST(GraphFixture, adjacent_vertices_2)
{
  using graph_type = typename TestFixture::graph_type;

  graph_type g = TestFixture::fake_undirected();
  auto adj_0 = adjacent_vertices(vertex(0, g), g);
  ASSERT_EQ(*(adj_0.first++), vertex(1, g));
  ASSERT_EQ(adj_0.first, adj_0.second);
  auto adj_2 = adjacent_vertices(vertex(2, g), g);
  ASSERT_EQ(*(adj_2.first++), vertex(1, g));
  ASSERT_EQ(adj_2.first, adj_2.second);
  auto adj_1 = adjacent_vertices(vertex(1, g), g);
  ASSERT_EQ(*(adj_1.first++), vertex(0, g));
  ASSERT_EQ(*(adj_1.first++), vertex(2, g));
  ASSERT_EQ(*(adj_1.first++), vertex(3, g));
  ASSERT_EQ(adj_1.first, adj_1.second);
  auto adj_3 = adjacent_vertices(vertex(3, g), g);
  ASSERT_EQ(*(adj_3.first++), vertex(1, g));
  ASSERT_EQ(*(adj_3.first++), vertex(4, g));
  ASSERT_EQ(*(adj_3.first++), vertex(5, g));
  ASSERT_EQ(adj_3.first, adj_3.second);
  auto adj_4 = adjacent_vertices(vertex(4, g), g);
  ASSERT_EQ(*(adj_4.first++), vertex(3, g));
  ASSERT_EQ(adj_4.first, adj_4.second);
  auto adj_5 = adjacent_vertices(vertex(5, g), g);
  ASSERT_EQ(*(adj_5.first++), vertex(3, g));
  ASSERT_EQ(adj_5.first, adj_5.second);
}

TYPED_TEST(GraphFixture, adjacent_vertices_3)
{
  using graph_type = typename TestFixture::graph_type;

  graph_type g = TestFixture::cycle();
  auto adj_0 = adjacent_vertices(vertex(0, g), g);
  ASSERT_EQ(*(adj_0.first++), vertex(1, g));
  ASSERT_EQ(adj_0.first, adj_0.second);
  auto adj_2 = adjacent_vertices(vertex(2, g), g);
  ASSERT_EQ(*(adj_2.first++), vertex(1, g));
  ASSERT_EQ(adj_2.first, adj_2.second);
  auto adj_1 = adjacent_vertices(vertex(1, g), g);
  ASSERT_EQ(*(adj_1.first++), vertex(3, g));
  ASSERT_EQ(adj_1.first, adj_1.second);
  auto adj_3 = adjacent_vertices(vertex(3, g), g);
  ASSERT_EQ(*(adj_3.first++), vertex(4, g));
  ASSERT_EQ(*(adj_3.first++), vertex(5, g));
  ASSERT_EQ(adj_3.first, adj_3.second);
  auto adj_4 = adjacent_vertices(vertex(4, g), g);
  ASSERT_EQ(adj_4.first, adj_4.second);
  auto adj_5 = adjacent_vertices(vertex(5, g), g);
  ASSERT_EQ(*(adj_5.first++), vertex(0, g));
  ASSERT_EQ(adj_5.first, adj_5.second);
}

TYPED_TEST(GraphFixture, adjacent_vertices_4)
{
  using graph_type = typename TestFixture::graph_type;

  graph_type g = TestFixture::self_cycle();
  auto adj_0 = adjacent_vertices(vertex(0, g), g);
  ASSERT_EQ(*(adj_0.first++), vertex(0, g));
  ASSERT_EQ(*(adj_0.first++), vertex(1, g));
  //  ASSERT_EQ(adj_0.first, adj_0.second); // Commented out because boost fails, unclear why
  auto adj_1 = adjacent_vertices(vertex(1, g), g);
  ASSERT_EQ(adj_1.first, adj_1.second);
}

TYPED_TEST(GraphFixture, edges_0)
{
  using graph_type = typename TestFixture::graph_type;

  graph_type g = TestFixture::empty_graph();
  auto e = edges(g);
  ASSERT_EQ(e.first, e.second);
}

TYPED_TEST(GraphFixture, edges_1)
{
  using graph_type = typename TestFixture::graph_type;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g = TestFixture::dag();
  auto e = edges(g);
  std::set<edge_descriptor> valid_edges = {
    edge(vertex(0, g), vertex(1, g), g).first, edge(vertex(2, g), vertex(1, g), g).first,
    edge(vertex(1, g), vertex(3, g), g).first, edge(vertex(1, g), vertex(3, g), g).first,
    edge(vertex(3, g), vertex(4, g), g).first, edge(vertex(3, g), vertex(5, g), g).first
  };
  while (e.first != e.second)
  {
    ASSERT_FALSE(valid_edges.empty());
    ASSERT_TRUE(valid_edges.count(*(e.first)) == 1);
    valid_edges.erase(*e.first);
    ++e.first;
  }
}

TYPED_TEST(GraphFixture, edges_2)
{
  using graph_type = typename TestFixture::graph_type;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g = TestFixture::fake_undirected();
  auto e = edges(g);
  std::set<edge_descriptor> valid_edges = {
    edge(vertex(0, g), vertex(1, g), g).first, edge(vertex(2, g), vertex(1, g), g).first,
    edge(vertex(1, g), vertex(3, g), g).first, edge(vertex(1, g), vertex(3, g), g).first,
    edge(vertex(3, g), vertex(4, g), g).first, edge(vertex(3, g), vertex(5, g), g).first,

    edge(vertex(1, g), vertex(0, g), g).first, edge(vertex(1, g), vertex(2, g), g).first,
    edge(vertex(3, g), vertex(1, g), g).first, edge(vertex(3, g), vertex(1, g), g).first,
    edge(vertex(4, g), vertex(3, g), g).first, edge(vertex(5, g), vertex(3, g), g).first
  };
  while (e.first != e.second)
  {
    ASSERT_FALSE(valid_edges.empty());
    ASSERT_TRUE(valid_edges.count(*(e.first)) == 1);
    valid_edges.erase(*e.first);
    ++e.first;
  }
}

TYPED_TEST(GraphFixture, edges_3)
{
  using graph_type = typename TestFixture::graph_type;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g = TestFixture::cycle();
  auto e = edges(g);
  std::set<edge_descriptor> valid_edges = {
    edge(vertex(0, g), vertex(1, g), g).first, edge(vertex(2, g), vertex(1, g), g).first,
    edge(vertex(1, g), vertex(3, g), g).first, edge(vertex(1, g), vertex(3, g), g).first,
    edge(vertex(3, g), vertex(4, g), g).first, edge(vertex(3, g), vertex(5, g), g).first,
    edge(vertex(5, g), vertex(0, g), g).first
  };
  while (e.first != e.second)
  {
    ASSERT_FALSE(valid_edges.empty());
    ASSERT_TRUE(valid_edges.count(*(e.first)) == 1);
    valid_edges.erase(*e.first);
    ++e.first;
  }
}

TYPED_TEST(GraphFixture, edges_4)
{
  using graph_type = typename TestFixture::graph_type;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g = TestFixture::self_cycle();
  auto e = edges(g);
  std::set<edge_descriptor> valid_edges = { edge(vertex(0, g), vertex(1, g), g).first,
                                            edge(vertex(0, g), vertex(0, g), g).first };
  while (e.first != e.second)
  {
    ASSERT_FALSE(valid_edges.empty());
    ASSERT_TRUE(valid_edges.count(*(e.first)) == 1);
    valid_edges.erase(*e.first);
    ++e.first;
  }
}

TYPED_TEST(GraphFixture, source_target_1)
{
  using graph_type = typename TestFixture::graph_type;
  using edge_descriptor = typename TestFixture::edge_descriptor;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g = TestFixture::dag();

  std::vector<edge_descriptor> valid_edges = {
    edge(vertex(0, g), vertex(1, g), g).first, edge(vertex(2, g), vertex(1, g), g).first,
    edge(vertex(1, g), vertex(3, g), g).first, edge(vertex(1, g), vertex(3, g), g).first,
    edge(vertex(3, g), vertex(4, g), g).first, edge(vertex(3, g), vertex(5, g), g).first
  };
  std::vector<vertex_descriptor> sources = { vertex(0, g), vertex(2, g), vertex(1, g), vertex(1, g),
                                             vertex(3, g), vertex(3, g), vertex(3, g) };
  std::vector<vertex_descriptor> targets = { vertex(1, g), vertex(1, g), vertex(3, g),
                                             vertex(3, g), vertex(4, g), vertex(5, g) };
  for (int i = 0; i < static_cast<int>(valid_edges.size()); ++i)
  {
    auto e = valid_edges[i];
    auto s = sources[i];
    auto t = targets[i];
    ASSERT_EQ(source(e, g), s);
    ASSERT_EQ(target(e, g), t);
  }
}
